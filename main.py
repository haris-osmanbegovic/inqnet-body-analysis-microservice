import os

from flask import Flask, render_template, request, redirect
from werkzeug.utils import secure_filename

from algorithms.main import analyse_image

UPLOAD_FOLDER = './uploads'
STATIC_FOLDER = './static'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)
app.secret_key = 'pragmacijo'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/analyse', methods=['POST'])
def analyse():
    try:
        # check if the post request has the image part
        if 'image' not in request.files:
            return redirect('/')

        image = request.files['image']
        algorithm = request.form['algorithm']

        if image.filename == '':
            return redirect('/')

        if image and allowed_file(image.filename):
            filename = secure_filename(image.filename)
            image_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            image.save(image_path)

            success, result = analyse_image(algorithm, image_path)
            if success:
                return render_template('result.html', image_path=result)
            else:
                # an error occurred, show the error message
                return result
    except Exception as e:
        print(e)
        return 'An error occurred, try again'


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
