import sys

import cv2

sys.path.append('/usr/local/python')
from openpose import pyopenpose as op


def get_points_from_image(image_path):
    # parameters
    params = dict()
    params["model_folder"] = "./openpose_models/"
    params["face"] = True
    params["hand"] = True

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()

    # Process Image
    datum = op.Datum()
    imageToProcess = cv2.imread(image_path)
    datum.cvInputData = imageToProcess
    opWrapper.emplaceAndPop([datum])

    return datum.cvOutputData
