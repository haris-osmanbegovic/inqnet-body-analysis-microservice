import os
import uuid

import cv2

from flask import url_for

from algorithms.tensorflow_openpose import get_points_from_image as tf_from_image
from algorithms.basic_openpose import get_points_from_image as op_from_image

STATIC_FOLDER = './static'


def analyse_image(algorithm, image_path):
    try:
        if algorithm == 'tf-openpose':
            data = tf_from_image(image_path)
        elif algorithm == 'openpose':
            data = op_from_image(image_path)
        else:
            raise KeyError('Specified algorithm not supported.')

        # save the result as image
        title = str(uuid.uuid4()) + '.jpg'
        image_path = os.path.join(STATIC_FOLDER, title)
        cv2.imwrite(image_path, data)
        result_path = url_for('static', filename=title)
        return True, result_path
    except Exception as e:
        print(e)
        return False, str(e)
